# Practical Generics

Miki Tebeka
<i class="far fa-envelope"></i> [miki@353solutions.com](mailto:miki@353solutions.com), <i class="fab fa-twitter"></i> [@tebeka](https://twitter.com/tebeka), <i class="fab fa-linkedin-in"></i> [mikitebeka](https://www.linkedin.com/in/mikitebeka/), <i class="fab fa-blogger-b"></i> [blog](https://www.ardanlabs.com/blog/)

#### Shameless Plugs

- [Go Essential Training](https://www.linkedin.com/learning/go-essential-training/) - LinkedIn Learning
    - [Rest of classes](https://www.linkedin.com/learning/instructors/miki-tebeka)
- [Go Brain Teasers](https://pragprog.com/titles/d-gobrain/go-brain-teasers/) book

---

### Links

- [When to use generics](https://go.dev/blog/when-generics)
- [Generics tutorial](https://go.dev/doc/tutorial/generics)
- [An Introduction to Generics](https://go.dev/blog/intro-generics)
- [All your comparable types](https://go.dev/blog/comparable)
    - [comparable](https://pkg.go.dev/builtin#comparable) built-in type
- [Generics can make your Go code slower](https://planetscale.com/blog/generics-can-make-your-go-code-slower)
- [Moving Average](https://en.wikipedia.org/wiki/Moving_average)
- Packages
    - [exp/constraints](https://pkg.go.dev/golang.org/x/exp/constraints)
    - [exp/maps](https://pkg.go.dev/golang.org/x/exp/maps)
    - [exp/slices](https://pkg.go.dev/golang.org/x/exp/slices)
    - [hashicorp/golang-lru](https://pkg.go.dev/github.com/hashicorp/golang-lru/v2)
    - [lo](https://github.com/samber/lo)

### Data & Other

- [Rolling Mean](_extra/rolling.html)

### Setting Up

Bring your laptop, we're going to write code together.

Make sure you have the following installed:
- [Go SDK](https://go.dev/dl/) installed
- IDE such as [VSCode](https://code.visualstudio.com/) with the [Go extension](https://marketplace.visualstudio.com/items?itemName=golang.Go) or [GoLand](https://www.jetbrains.com/go/)
- `git`
