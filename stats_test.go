package stats

import (
	"testing"

	"github.com/stretchr/testify/require"
)

func TestMaxInts(t *testing.T) {
	values := []int{3, 1, 2}
	m, err := MaxInts(values)
	require.NoError(t, err)
	require.Equal(t, 3, m)
}

func TestMaxIntsEmpty(t *testing.T) {
	_, err := MaxInts(nil)
	require.Error(t, err)
}

func TestMaxFloat64s(t *testing.T) {
	values := []float64{3, 1, 2}
	m, err := MaxFloat64s(values)
	require.NoError(t, err)
	require.Equal(t, 3.0, m)
}

func TestMaxFloat64sEmpty(t *testing.T) {
	_, err := MaxFloat64s(nil)
	require.Error(t, err)
}
